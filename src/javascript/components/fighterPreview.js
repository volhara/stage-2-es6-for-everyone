import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  if(fighter)
  {
    fighterElement.append(createFighterImage(fighter));
    fighterElement.append(createFighterInfo(fighter));
  }
  return fighterElement;
}

export function createFighterInfo(fighter) {
  const { name, health, attack, defense } = fighter;
  const infoElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___info-container'
  });
  const spanElement = createElement({
    tagName: 'span'
  });
  spanElement.innerText = `${name}
  health: ${health}
  attack: ${attack}
  defense: ${defense}`;
  infoElement.append(spanElement);
  return infoElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
