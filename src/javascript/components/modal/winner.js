import {showModal} from './modal';
import {createElement} from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  const title = `Congratulations!`;
  const bodyResult=createElement({
    tagName:'div',
    className:'modal-body'
  });

  const { source, name } = fighter;
  
  const attributes = { 
    src: fighter.source, 
    title: fighter.name,
    alt: fighter.name 
  };
  const winnerImg=createElement({
    tagName:'img',
    className:'modal-body',
    attributes
  });
  const winnerText = createElement({ tagName: 'H1', className: 'modal-body' });
  winnerText.innerText = fighter.name + " Wins!!!";

  bodyResult.append(winnerText);
  bodyResult.append(winnerImg);

  const result = {
    title,
    bodyElement: bodyResult,
    onClose: () => {
      window.location.reload();
    },
  };
  showModal(result);
}
